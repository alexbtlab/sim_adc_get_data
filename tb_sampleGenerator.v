`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.04.2020 09:54:36
// Design Name: 
// Module Name: tb_sampleGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_sampleGenerator();
  reg [15:0]FrameSize_0;
  wire WR_0;
  wire [15:0]adr_ram_0;
  reg clk_0;
  wire clk_write_read_0;
  wire [31:0]data_adc;
  wire [31:0]o_data_0;
  wire ready_0;
  reg reset_0;

initial begin
    
    
    clk_0 = 0;
    forever #5 clk_0 = ~clk_0;
    
end
initial begin
    
    reset_0 = 0;
    #100 reset_0 = 1;
    
end
    
   design_1_wrapper DUT
   (
   
    .WR_0(WR_0),
    .adr_ram_0(adr_ram_0),
    .clk_0(clk_0),
    .clk_write_read_0(clk_write_read_0),
    .data_adc(data_adc),
    .o_data_0(o_data_0),
    .ready_0(ready_0),
    .reset_0(reset_0)
    );
    
  

endmodule
