//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sat Jun 13 10:42:49 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target ADC2AXI_sub.bd
//Design      : ADC2AXI_sub
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "ADC2AXI_sub,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=ADC2AXI_sub,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=3,numReposBlks=3,numNonXlnxBlks=2,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "ADC2AXI_sub.hwdef" *) 
module ADC2AXI_sub
   (FrameSize,
    M00_AXIS_tdata,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tvalid,
    data_ready,
    i_data,
    m00_axis_areset,
    main_clk,
    reset_ram);
  input [15:0]FrameSize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, FREQ_HZ 100000000, HAS_TKEEP 0, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 1, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0" *) output [31:0]M00_AXIS_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS " *) output M00_AXIS_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS " *) input M00_AXIS_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS " *) output [3:0]M00_AXIS_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS " *) output M00_AXIS_tvalid;
  input data_ready;
  input [31:0]i_data;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.M00_AXIS_ARESET RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.M00_AXIS_ARESET, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input m00_axis_areset;
  input main_clk;
  input reset_ram;

  wire [15:0]FrameSize_0_1;
  wire data_ready_0_1;
  wire [31:0]i_data_0_1;
  wire m00_axis_aresetn_0_1;
  wire main_clk_0_1;
  wire [31:0]ram_4096_0_o_data;
  wire ram_control_0_WR;
  wire [15:0]ram_control_0_adr_ram;
  wire ram_control_0_clk_100MHz_AXI;
  wire ram_control_0_clk_write_read;
  wire reset_0_1;
  wire [31:0]sampleGenerator_0_M00_AXIS_TDATA;
  wire sampleGenerator_0_M00_AXIS_TLAST;
  wire sampleGenerator_0_M00_AXIS_TREADY;
  wire [3:0]sampleGenerator_0_M00_AXIS_TSTRB;
  wire sampleGenerator_0_M00_AXIS_TVALID;

  assign FrameSize_0_1 = FrameSize[15:0];
  assign M00_AXIS_tdata[31:0] = sampleGenerator_0_M00_AXIS_TDATA;
  assign M00_AXIS_tlast = sampleGenerator_0_M00_AXIS_TLAST;
  assign M00_AXIS_tstrb[3:0] = sampleGenerator_0_M00_AXIS_TSTRB;
  assign M00_AXIS_tvalid = sampleGenerator_0_M00_AXIS_TVALID;
  assign data_ready_0_1 = data_ready;
  assign i_data_0_1 = i_data[31:0];
  assign m00_axis_aresetn_0_1 = m00_axis_areset;
  assign main_clk_0_1 = main_clk;
  assign reset_0_1 = reset_ram;
  assign sampleGenerator_0_M00_AXIS_TREADY = M00_AXIS_tready;
  ADC2AXI_sub_ram_4096_0_0 ram_4096_0
       (.i_addr(ram_control_0_adr_ram),
        .i_clk(ram_control_0_clk_write_read),
        .i_data(i_data_0_1),
        .i_write(ram_control_0_WR),
        .o_data(ram_4096_0_o_data));
  ADC2AXI_sub_ram_control_0_0 ram_control_0
       (.FrameSize(FrameSize_0_1),
        .WR(ram_control_0_WR),
        .adr_ram(ram_control_0_adr_ram),
        .clk_100MHz_AXI(ram_control_0_clk_100MHz_AXI),
        .clk_write_read(ram_control_0_clk_write_read),
        .data_ready(data_ready_0_1),
        .main_clk(main_clk_0_1),
        .reset(reset_0_1));
  ADC2AXI_sub_sampleGenerator_0_0 sampleGenerator_0
       (.FrameSize(FrameSize_0_1),
        .data_from_ram(ram_4096_0_o_data),
        .en_AXIclk(ram_control_0_WR),
        .m00_axis_aclk(ram_control_0_clk_100MHz_AXI),
        .m00_axis_aresetn(m00_axis_aresetn_0_1),
        .m00_axis_tdata(sampleGenerator_0_M00_AXIS_TDATA),
        .m00_axis_tlast(sampleGenerator_0_M00_AXIS_TLAST),
        .m00_axis_tready(sampleGenerator_0_M00_AXIS_TREADY),
        .m00_axis_tstrb(sampleGenerator_0_M00_AXIS_TSTRB),
        .m00_axis_tvalid(sampleGenerator_0_M00_AXIS_TVALID),
        .main_clk_100MHz(main_clk_0_1));
endmodule
