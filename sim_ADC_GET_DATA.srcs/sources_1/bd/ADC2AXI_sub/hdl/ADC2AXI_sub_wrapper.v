//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sat Jun 13 10:42:49 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target ADC2AXI_sub_wrapper.bd
//Design      : ADC2AXI_sub_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module ADC2AXI_sub_wrapper
   (FrameSize,
    M00_AXIS_tdata,
    M00_AXIS_tlast,
    M00_AXIS_tready,
    M00_AXIS_tstrb,
    M00_AXIS_tvalid,
    data_ready,
    i_data,
    m00_axis_areset,
    main_clk,
    reset_ram);
  input [15:0]FrameSize;
  output [31:0]M00_AXIS_tdata;
  output M00_AXIS_tlast;
  input M00_AXIS_tready;
  output [3:0]M00_AXIS_tstrb;
  output M00_AXIS_tvalid;
  input data_ready;
  input [31:0]i_data;
  input m00_axis_areset;
  input main_clk;
  input reset_ram;

  wire [15:0]FrameSize;
  wire [31:0]M00_AXIS_tdata;
  wire M00_AXIS_tlast;
  wire M00_AXIS_tready;
  wire [3:0]M00_AXIS_tstrb;
  wire M00_AXIS_tvalid;
  wire data_ready;
  wire [31:0]i_data;
  wire m00_axis_areset;
  wire main_clk;
  wire reset_ram;

  ADC2AXI_sub ADC2AXI_sub_i
       (.FrameSize(FrameSize),
        .M00_AXIS_tdata(M00_AXIS_tdata),
        .M00_AXIS_tlast(M00_AXIS_tlast),
        .M00_AXIS_tready(M00_AXIS_tready),
        .M00_AXIS_tstrb(M00_AXIS_tstrb),
        .M00_AXIS_tvalid(M00_AXIS_tvalid),
        .data_ready(data_ready),
        .i_data(i_data),
        .m00_axis_areset(m00_axis_areset),
        .main_clk(main_clk),
        .reset_ram(reset_ram));
endmodule
