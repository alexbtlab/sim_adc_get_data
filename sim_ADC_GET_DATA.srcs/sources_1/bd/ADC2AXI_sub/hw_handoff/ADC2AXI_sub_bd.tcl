
################################################################
# This is a generated script based on design: ADC2AXI_sub
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source ADC2AXI_sub_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7a100tfgg484-1
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name ADC2AXI_sub

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set M00_AXIS [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS ]


  # Create ports
  set FrameSize [ create_bd_port -dir I -from 15 -to 0 FrameSize ]
  set data_ready [ create_bd_port -dir I data_ready ]
  set i_data [ create_bd_port -dir I -from 31 -to 0 i_data ]
  set m00_axis_areset [ create_bd_port -dir I -type rst m00_axis_areset ]
  set main_clk [ create_bd_port -dir I main_clk ]
  set reset_ram [ create_bd_port -dir I reset_ram ]

  # Create instance: ram_4096_0, and set properties
  set ram_4096_0 [ create_bd_cell -type ip -vlnv bt.local:user:ram_4096:1.0 ram_4096_0 ]

  # Create instance: ram_control_0, and set properties
  set ram_control_0 [ create_bd_cell -type ip -vlnv bt.local:user:ram_control:1.0 ram_control_0 ]

  # Create instance: sampleGenerator_0, and set properties
  set sampleGenerator_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:sampleGenerator:1.0 sampleGenerator_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net sampleGenerator_0_M00_AXIS [get_bd_intf_ports M00_AXIS] [get_bd_intf_pins sampleGenerator_0/M00_AXIS]

  # Create port connections
  connect_bd_net -net FrameSize_0_1 [get_bd_ports FrameSize] [get_bd_pins ram_control_0/FrameSize] [get_bd_pins sampleGenerator_0/FrameSize]
  connect_bd_net -net data_ready_0_1 [get_bd_ports data_ready] [get_bd_pins ram_control_0/data_ready]
  connect_bd_net -net i_data_0_1 [get_bd_ports i_data] [get_bd_pins ram_4096_0/i_data]
  connect_bd_net -net m00_axis_aresetn_0_1 [get_bd_ports m00_axis_areset] [get_bd_pins sampleGenerator_0/m00_axis_aresetn]
  connect_bd_net -net main_clk_0_1 [get_bd_ports main_clk] [get_bd_pins ram_control_0/main_clk] [get_bd_pins sampleGenerator_0/main_clk_100MHz]
  connect_bd_net -net ram_4096_0_o_data [get_bd_pins ram_4096_0/o_data] [get_bd_pins sampleGenerator_0/data_from_ram]
  connect_bd_net -net ram_control_0_WR [get_bd_pins ram_4096_0/i_write] [get_bd_pins ram_control_0/WR] [get_bd_pins sampleGenerator_0/en_AXIclk]
  connect_bd_net -net ram_control_0_adr_ram [get_bd_pins ram_4096_0/i_addr] [get_bd_pins ram_control_0/adr_ram]
  connect_bd_net -net ram_control_0_clk_100MHz_AXI [get_bd_pins ram_control_0/clk_100MHz_AXI] [get_bd_pins sampleGenerator_0/m00_axis_aclk]
  connect_bd_net -net ram_control_0_clk_write_read [get_bd_pins ram_4096_0/i_clk] [get_bd_pins ram_control_0/clk_write_read]
  connect_bd_net -net reset_0_1 [get_bd_ports reset_ram] [get_bd_pins ram_control_0/reset]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


