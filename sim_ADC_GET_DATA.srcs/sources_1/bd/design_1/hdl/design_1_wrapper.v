//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Fri Jun 12 00:07:49 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (WR_0,
    adr_ram_0,
    clk_0,
    clk_write_read_0,
    data_adc,
    o_data_0,
    ready_0,
    reset_0);
  output WR_0;
  output [15:0]adr_ram_0;
  input clk_0;
  output clk_write_read_0;
  output [31:0]data_adc;
  output [31:0]o_data_0;
  output ready_0;
  input reset_0;

  wire WR_0;
  wire [15:0]adr_ram_0;
  wire clk_0;
  wire clk_write_read_0;
  wire [31:0]data_adc;
  wire [31:0]o_data_0;
  wire ready_0;
  wire reset_0;

  design_1 design_1_i
       (.WR_0(WR_0),
        .adr_ram_0(adr_ram_0),
        .clk_0(clk_0),
        .clk_write_read_0(clk_write_read_0),
        .data_adc(data_adc),
        .o_data_0(o_data_0),
        .ready_0(ready_0),
        .reset_0(reset_0));
endmodule
