// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  9 16:09:28 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/project/sim_ADC_GET_DATA/sim_ADC_GET_DATA/sim_ADC_GET_DATA.srcs/sources_1/bd/design_1/ip/design_1_ram_4096_0_0/design_1_ram_4096_0_0_stub.v
// Design      : design_1_ram_4096_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ram,Vivado 2019.1" *)
module design_1_ram_4096_0_0(i_clk, i_addr, i_write, i_data, o_data)
/* synthesis syn_black_box black_box_pad_pin="i_clk,i_addr[15:0],i_write,i_data[31:0],o_data[31:0]" */;
  input i_clk;
  input [15:0]i_addr;
  input i_write;
  input [31:0]i_data;
  output [31:0]o_data;
endmodule
