module counter(

		input wire clk,
		input wire reset,
		input wire [15:0] FrameSize,
		output wire ready,
		output wire [31:0] count
);

reg [15:0] cnt = 0;

assign count = cnt;
assign ready = clk;



always @(posedge clk) begin

    if(!reset) begin
        cnt <= 32'h0000;
    end
    else begin
        if(cnt == (FrameSize+1)) begin
            cnt <= 32'h0000;
        end
        else begin
            cnt <= cnt + 1;
        end
    end 
           
end
endmodule