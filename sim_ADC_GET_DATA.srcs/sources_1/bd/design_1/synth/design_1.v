//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Fri Jun 12 00:07:49 2020
//Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=7,numReposBlks=7,numNonXlnxBlks=3,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (WR_0,
    adr_ram_0,
    clk_0,
    clk_write_read_0,
    data_adc,
    o_data_0,
    ready_0,
    reset_0);
  output WR_0;
  output [15:0]adr_ram_0;
  input clk_0;
  output clk_write_read_0;
  output [31:0]data_adc;
  output [31:0]o_data_0;
  output ready_0;
  input reset_0;

  wire clk_0_1;
  wire clk_wiz_0_clk_out1;
  wire clk_wiz_1_clk_out1;
  wire [31:0]counter_0_count;
  wire counter_0_ready;
  wire [31:0]ram_4096_0_o_data;
  wire ram_control_0_WR;
  wire [15:0]ram_control_0_adr_ram;
  wire ram_control_0_clk_100MHz_AXI;
  wire ram_control_0_clk_write_read;
  wire reset_0_1;
  wire [15:0]xlconstant_0_dout;

  assign WR_0 = ram_control_0_WR;
  assign adr_ram_0[15:0] = ram_control_0_adr_ram;
  assign clk_0_1 = clk_0;
  assign clk_write_read_0 = ram_control_0_clk_write_read;
  assign data_adc[31:0] = counter_0_count;
  assign o_data_0[31:0] = ram_4096_0_o_data;
  assign ready_0 = counter_0_ready;
  assign reset_0_1 = reset_0;
  design_1_clk_wiz_0_0 clk_wiz_0
       (.clk_in1(clk_0_1),
        .clk_out1(clk_wiz_0_clk_out1));
  design_1_clk_wiz_1_0 clk_wiz_1
       (.clk_in1(clk_wiz_0_clk_out1),
        .clk_out1(clk_wiz_1_clk_out1));
  design_1_counter_0_1 counter_0
       (.FrameSize(xlconstant_0_dout),
        .clk(clk_wiz_1_clk_out1),
        .count(counter_0_count),
        .ready(counter_0_ready),
        .reset(reset_0_1));
  design_1_ram_4096_0_0 ram_4096_0
       (.i_addr(ram_control_0_adr_ram),
        .i_clk(ram_control_0_clk_write_read),
        .i_data(counter_0_count),
        .i_write(ram_control_0_WR),
        .o_data(ram_4096_0_o_data));
  design_1_ram_control_0_0 ram_control_0
       (.FrameSize(xlconstant_0_dout),
        .WR(ram_control_0_WR),
        .adr_ram(ram_control_0_adr_ram),
        .clk_100MHz_AXI(ram_control_0_clk_100MHz_AXI),
        .clk_write_read(ram_control_0_clk_write_read),
        .data_ready(counter_0_ready),
        .main_clk(clk_0_1),
        .reset(reset_0_1));
  design_1_sampleGenerator_0_1 sampleGenerator_0
       (.FrameSize(xlconstant_0_dout),
        .data_from_ram(ram_4096_0_o_data),
        .en_AXIclk(ram_control_0_WR),
        .m00_axis_aclk(ram_control_0_clk_100MHz_AXI),
        .m00_axis_aresetn(reset_0_1),
        .m00_axis_tready(1'b1),
        .main_clk_100MHz(clk_0_1));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule
