-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  9 16:08:52 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ram_control_0_0_sim_netlist.vhdl
-- Design      : design_1_ram_control_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram_control is
  port (
    UNCONN_OUT : out STD_LOGIC;
    adr_ram : out STD_LOGIC_VECTOR ( 0 to 0 );
    main_clk : in STD_LOGIC;
    data_ready : in STD_LOGIC;
    reset : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram_control;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram_control is
  signal \^unconn_out\ : STD_LOGIC;
  signal count_data_ready_from_ADC : STD_LOGIC;
  signal count_data_ready_from_ADC0 : STD_LOGIC;
  signal \count_data_ready_from_ADC0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count_data_ready_from_ADC0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count_data_ready_from_ADC0_carry__0_n_3\ : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_i_1_n_0 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_i_2_n_0 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_i_3_n_0 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_i_4_n_0 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_n_0 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_n_1 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_n_2 : STD_LOGIC;
  signal count_data_ready_from_ADC0_carry_n_3 : STD_LOGIC;
  signal count_data_ready_from_ADC_i_1_n_0 : STD_LOGIC;
  signal count_main_clk : STD_LOGIC;
  signal count_main_clk0 : STD_LOGIC;
  signal \count_main_clk0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count_main_clk0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count_main_clk0_carry__0_n_3\ : STD_LOGIC;
  signal count_main_clk0_carry_i_1_n_0 : STD_LOGIC;
  signal count_main_clk0_carry_i_2_n_0 : STD_LOGIC;
  signal count_main_clk0_carry_i_3_n_0 : STD_LOGIC;
  signal count_main_clk0_carry_i_4_n_0 : STD_LOGIC;
  signal count_main_clk0_carry_n_0 : STD_LOGIC;
  signal count_main_clk0_carry_n_1 : STD_LOGIC;
  signal count_main_clk0_carry_n_2 : STD_LOGIC;
  signal count_main_clk0_carry_n_3 : STD_LOGIC;
  signal count_main_clk_i_1_n_0 : STD_LOGIC;
  signal \state_WR_inferred__0/i__n_0\ : STD_LOGIC;
  signal state_WR_n_0 : STD_LOGIC;
  signal NLW_count_data_ready_from_ADC0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count_data_ready_from_ADC0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_data_ready_from_ADC0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_count_main_clk0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count_main_clk0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_main_clk0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  UNCONN_OUT <= \^unconn_out\;
\adr_ram__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => count_data_ready_from_ADC,
      I1 => count_main_clk,
      I2 => \^unconn_out\,
      O => adr_ram(0)
    );
count_data_ready_from_ADC0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count_data_ready_from_ADC0_carry_n_0,
      CO(2) => count_data_ready_from_ADC0_carry_n_1,
      CO(1) => count_data_ready_from_ADC0_carry_n_2,
      CO(0) => count_data_ready_from_ADC0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_count_data_ready_from_ADC0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => count_data_ready_from_ADC0_carry_i_1_n_0,
      S(2) => count_data_ready_from_ADC0_carry_i_2_n_0,
      S(1) => count_data_ready_from_ADC0_carry_i_3_n_0,
      S(0) => count_data_ready_from_ADC0_carry_i_4_n_0
    );
\count_data_ready_from_ADC0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count_data_ready_from_ADC0_carry_n_0,
      CO(3 downto 2) => \NLW_count_data_ready_from_ADC0_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => count_data_ready_from_ADC0,
      CO(0) => \count_data_ready_from_ADC0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_count_data_ready_from_ADC0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \count_data_ready_from_ADC0_carry__0_i_1_n_0\,
      S(0) => \count_data_ready_from_ADC0_carry__0_i_2_n_0\
    );
\count_data_ready_from_ADC0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \count_data_ready_from_ADC0_carry__0_i_1_n_0\
    );
\count_data_ready_from_ADC0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(14),
      I1 => FrameSize(13),
      I2 => FrameSize(12),
      O => \count_data_ready_from_ADC0_carry__0_i_2_n_0\
    );
count_data_ready_from_ADC0_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(11),
      I1 => FrameSize(10),
      I2 => FrameSize(9),
      O => count_data_ready_from_ADC0_carry_i_1_n_0
    );
count_data_ready_from_ADC0_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(8),
      I1 => FrameSize(7),
      I2 => FrameSize(6),
      O => count_data_ready_from_ADC0_carry_i_2_n_0
    );
count_data_ready_from_ADC0_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(5),
      I1 => FrameSize(4),
      I2 => FrameSize(3),
      O => count_data_ready_from_ADC0_carry_i_3_n_0
    );
count_data_ready_from_ADC0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0009"
    )
        port map (
      I0 => count_data_ready_from_ADC,
      I1 => FrameSize(0),
      I2 => FrameSize(2),
      I3 => FrameSize(1),
      O => count_data_ready_from_ADC0_carry_i_4_n_0
    );
count_data_ready_from_ADC_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0848"
    )
        port map (
      I0 => count_data_ready_from_ADC,
      I1 => reset,
      I2 => \^unconn_out\,
      I3 => count_data_ready_from_ADC0,
      O => count_data_ready_from_ADC_i_1_n_0
    );
count_data_ready_from_ADC_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_ready,
      CE => '1',
      D => count_data_ready_from_ADC_i_1_n_0,
      Q => count_data_ready_from_ADC,
      R => '0'
    );
count_main_clk0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count_main_clk0_carry_n_0,
      CO(2) => count_main_clk0_carry_n_1,
      CO(1) => count_main_clk0_carry_n_2,
      CO(0) => count_main_clk0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_count_main_clk0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => count_main_clk0_carry_i_1_n_0,
      S(2) => count_main_clk0_carry_i_2_n_0,
      S(1) => count_main_clk0_carry_i_3_n_0,
      S(0) => count_main_clk0_carry_i_4_n_0
    );
\count_main_clk0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count_main_clk0_carry_n_0,
      CO(3 downto 2) => \NLW_count_main_clk0_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => count_main_clk0,
      CO(0) => \count_main_clk0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_count_main_clk0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \count_main_clk0_carry__0_i_1_n_0\,
      S(0) => \count_main_clk0_carry__0_i_2_n_0\
    );
\count_main_clk0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \count_main_clk0_carry__0_i_1_n_0\
    );
\count_main_clk0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(14),
      I1 => FrameSize(13),
      I2 => FrameSize(12),
      O => \count_main_clk0_carry__0_i_2_n_0\
    );
count_main_clk0_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(11),
      I1 => FrameSize(10),
      I2 => FrameSize(9),
      O => count_main_clk0_carry_i_1_n_0
    );
count_main_clk0_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(8),
      I1 => FrameSize(7),
      I2 => FrameSize(6),
      O => count_main_clk0_carry_i_2_n_0
    );
count_main_clk0_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => FrameSize(5),
      I1 => FrameSize(4),
      I2 => FrameSize(3),
      O => count_main_clk0_carry_i_3_n_0
    );
count_main_clk0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0009"
    )
        port map (
      I0 => count_main_clk,
      I1 => FrameSize(0),
      I2 => FrameSize(2),
      I3 => FrameSize(1),
      O => count_main_clk0_carry_i_4_n_0
    );
count_main_clk_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8804"
    )
        port map (
      I0 => count_main_clk,
      I1 => reset,
      I2 => count_main_clk0,
      I3 => \^unconn_out\,
      O => count_main_clk_i_1_n_0
    );
count_main_clk_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk,
      CE => '1',
      D => count_main_clk_i_1_n_0,
      Q => count_main_clk,
      R => '0'
    );
state_WR: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => count_main_clk0,
      I1 => \^unconn_out\,
      I2 => reset,
      O => state_WR_n_0
    );
\state_WR_inferred__0/i_\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2F"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => count_data_ready_from_ADC0,
      I2 => reset,
      O => \state_WR_inferred__0/i__n_0\
    );
state_WR_reg: unisim.vcomponents.FDRE
     port map (
      C => main_clk,
      CE => '1',
      D => state_WR_n_0,
      Q => \^unconn_out\,
      R => '0'
    );
\state_WR_reg__0\: unisim.vcomponents.FDRE
     port map (
      C => data_ready,
      CE => '1',
      D => \state_WR_inferred__0/i__n_0\,
      Q => \^unconn_out\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    main_clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    data_ready : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_write_read : out STD_LOGIC;
    adr_ram : out STD_LOGIC_VECTOR ( 15 downto 0 );
    WR : out STD_LOGIC;
    clk_100MHz_AXI : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ram_control_0_0,ram_control,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ram_control,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^wr\ : STD_LOGIC;
  signal \^adr_ram\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  WR <= \^wr\;
  adr_ram(15) <= \<const0>\;
  adr_ram(14) <= \<const0>\;
  adr_ram(13) <= \<const0>\;
  adr_ram(12) <= \<const0>\;
  adr_ram(11) <= \<const0>\;
  adr_ram(10) <= \<const0>\;
  adr_ram(9) <= \<const0>\;
  adr_ram(8) <= \<const0>\;
  adr_ram(7) <= \<const0>\;
  adr_ram(6) <= \<const0>\;
  adr_ram(5) <= \<const0>\;
  adr_ram(4) <= \<const0>\;
  adr_ram(3) <= \<const0>\;
  adr_ram(2) <= \<const0>\;
  adr_ram(1) <= \<const0>\;
  adr_ram(0) <= \^adr_ram\(0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
clk_100MHz_AXI_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => main_clk,
      I1 => \^wr\,
      O => clk_100MHz_AXI
    );
clk_write_read_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data_ready,
      I1 => \^wr\,
      I2 => main_clk,
      O => clk_write_read
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram_control
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      UNCONN_OUT => \^wr\,
      adr_ram(0) => \^adr_ram\(0),
      data_ready => data_ready,
      main_clk => main_clk,
      reset => reset
    );
end STRUCTURE;
