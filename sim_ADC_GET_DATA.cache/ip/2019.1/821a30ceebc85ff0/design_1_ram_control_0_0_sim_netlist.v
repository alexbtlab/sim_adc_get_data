// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  9 16:08:52 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ram_control_0_0_sim_netlist.v
// Design      : design_1_ram_control_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ram_control_0_0,ram_control,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ram_control,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (main_clk,
    reset,
    data_ready,
    FrameSize,
    clk_write_read,
    adr_ram,
    WR,
    clk_100MHz_AXI);
  input main_clk;
  input reset;
  input data_ready;
  input [15:0]FrameSize;
  output clk_write_read;
  output [15:0]adr_ram;
  output WR;
  output clk_100MHz_AXI;

  wire \<const0> ;
  wire [15:0]FrameSize;
  wire WR;
  wire [0:0]\^adr_ram ;
  wire clk_100MHz_AXI;
  wire clk_write_read;
  wire data_ready;
  wire main_clk;
  wire reset;

  assign adr_ram[15] = \<const0> ;
  assign adr_ram[14] = \<const0> ;
  assign adr_ram[13] = \<const0> ;
  assign adr_ram[12] = \<const0> ;
  assign adr_ram[11] = \<const0> ;
  assign adr_ram[10] = \<const0> ;
  assign adr_ram[9] = \<const0> ;
  assign adr_ram[8] = \<const0> ;
  assign adr_ram[7] = \<const0> ;
  assign adr_ram[6] = \<const0> ;
  assign adr_ram[5] = \<const0> ;
  assign adr_ram[4] = \<const0> ;
  assign adr_ram[3] = \<const0> ;
  assign adr_ram[2] = \<const0> ;
  assign adr_ram[1] = \<const0> ;
  assign adr_ram[0] = \^adr_ram [0];
  GND GND
       (.G(\<const0> ));
  LUT2 #(
    .INIT(4'h2)) 
    clk_100MHz_AXI_INST_0
       (.I0(main_clk),
        .I1(WR),
        .O(clk_100MHz_AXI));
  LUT3 #(
    .INIT(8'hB8)) 
    clk_write_read_INST_0
       (.I0(data_ready),
        .I1(WR),
        .I2(main_clk),
        .O(clk_write_read));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram_control inst
       (.FrameSize(FrameSize),
        .UNCONN_OUT(WR),
        .adr_ram(\^adr_ram ),
        .data_ready(data_ready),
        .main_clk(main_clk),
        .reset(reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram_control
   (UNCONN_OUT,
    adr_ram,
    main_clk,
    data_ready,
    reset,
    FrameSize);
  output UNCONN_OUT;
  output [0:0]adr_ram;
  input main_clk;
  input data_ready;
  input reset;
  input [15:0]FrameSize;

  wire [15:0]FrameSize;
  wire UNCONN_OUT;
  wire [0:0]adr_ram;
  wire count_data_ready_from_ADC;
  wire count_data_ready_from_ADC0;
  wire count_data_ready_from_ADC0_carry__0_i_1_n_0;
  wire count_data_ready_from_ADC0_carry__0_i_2_n_0;
  wire count_data_ready_from_ADC0_carry__0_n_3;
  wire count_data_ready_from_ADC0_carry_i_1_n_0;
  wire count_data_ready_from_ADC0_carry_i_2_n_0;
  wire count_data_ready_from_ADC0_carry_i_3_n_0;
  wire count_data_ready_from_ADC0_carry_i_4_n_0;
  wire count_data_ready_from_ADC0_carry_n_0;
  wire count_data_ready_from_ADC0_carry_n_1;
  wire count_data_ready_from_ADC0_carry_n_2;
  wire count_data_ready_from_ADC0_carry_n_3;
  wire count_data_ready_from_ADC_i_1_n_0;
  wire count_main_clk;
  wire count_main_clk0;
  wire count_main_clk0_carry__0_i_1_n_0;
  wire count_main_clk0_carry__0_i_2_n_0;
  wire count_main_clk0_carry__0_n_3;
  wire count_main_clk0_carry_i_1_n_0;
  wire count_main_clk0_carry_i_2_n_0;
  wire count_main_clk0_carry_i_3_n_0;
  wire count_main_clk0_carry_i_4_n_0;
  wire count_main_clk0_carry_n_0;
  wire count_main_clk0_carry_n_1;
  wire count_main_clk0_carry_n_2;
  wire count_main_clk0_carry_n_3;
  wire count_main_clk_i_1_n_0;
  wire data_ready;
  wire main_clk;
  wire reset;
  wire \state_WR_inferred__0/i__n_0 ;
  wire state_WR_n_0;
  wire [3:0]NLW_count_data_ready_from_ADC0_carry_O_UNCONNECTED;
  wire [3:2]NLW_count_data_ready_from_ADC0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_count_data_ready_from_ADC0_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_count_main_clk0_carry_O_UNCONNECTED;
  wire [3:2]NLW_count_main_clk0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_count_main_clk0_carry__0_O_UNCONNECTED;

  LUT3 #(
    .INIT(8'hAC)) 
    adr_ram__0
       (.I0(count_data_ready_from_ADC),
        .I1(count_main_clk),
        .I2(UNCONN_OUT),
        .O(adr_ram));
  CARRY4 count_data_ready_from_ADC0_carry
       (.CI(1'b0),
        .CO({count_data_ready_from_ADC0_carry_n_0,count_data_ready_from_ADC0_carry_n_1,count_data_ready_from_ADC0_carry_n_2,count_data_ready_from_ADC0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_data_ready_from_ADC0_carry_O_UNCONNECTED[3:0]),
        .S({count_data_ready_from_ADC0_carry_i_1_n_0,count_data_ready_from_ADC0_carry_i_2_n_0,count_data_ready_from_ADC0_carry_i_3_n_0,count_data_ready_from_ADC0_carry_i_4_n_0}));
  CARRY4 count_data_ready_from_ADC0_carry__0
       (.CI(count_data_ready_from_ADC0_carry_n_0),
        .CO({NLW_count_data_ready_from_ADC0_carry__0_CO_UNCONNECTED[3:2],count_data_ready_from_ADC0,count_data_ready_from_ADC0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_data_ready_from_ADC0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,count_data_ready_from_ADC0_carry__0_i_1_n_0,count_data_ready_from_ADC0_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count_data_ready_from_ADC0_carry__0_i_1
       (.I0(FrameSize[15]),
        .O(count_data_ready_from_ADC0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_data_ready_from_ADC0_carry__0_i_2
       (.I0(FrameSize[14]),
        .I1(FrameSize[13]),
        .I2(FrameSize[12]),
        .O(count_data_ready_from_ADC0_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_data_ready_from_ADC0_carry_i_1
       (.I0(FrameSize[11]),
        .I1(FrameSize[10]),
        .I2(FrameSize[9]),
        .O(count_data_ready_from_ADC0_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_data_ready_from_ADC0_carry_i_2
       (.I0(FrameSize[8]),
        .I1(FrameSize[7]),
        .I2(FrameSize[6]),
        .O(count_data_ready_from_ADC0_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_data_ready_from_ADC0_carry_i_3
       (.I0(FrameSize[5]),
        .I1(FrameSize[4]),
        .I2(FrameSize[3]),
        .O(count_data_ready_from_ADC0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h0009)) 
    count_data_ready_from_ADC0_carry_i_4
       (.I0(count_data_ready_from_ADC),
        .I1(FrameSize[0]),
        .I2(FrameSize[2]),
        .I3(FrameSize[1]),
        .O(count_data_ready_from_ADC0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h0848)) 
    count_data_ready_from_ADC_i_1
       (.I0(count_data_ready_from_ADC),
        .I1(reset),
        .I2(UNCONN_OUT),
        .I3(count_data_ready_from_ADC0),
        .O(count_data_ready_from_ADC_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    count_data_ready_from_ADC_reg
       (.C(data_ready),
        .CE(1'b1),
        .D(count_data_ready_from_ADC_i_1_n_0),
        .Q(count_data_ready_from_ADC),
        .R(1'b0));
  CARRY4 count_main_clk0_carry
       (.CI(1'b0),
        .CO({count_main_clk0_carry_n_0,count_main_clk0_carry_n_1,count_main_clk0_carry_n_2,count_main_clk0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_main_clk0_carry_O_UNCONNECTED[3:0]),
        .S({count_main_clk0_carry_i_1_n_0,count_main_clk0_carry_i_2_n_0,count_main_clk0_carry_i_3_n_0,count_main_clk0_carry_i_4_n_0}));
  CARRY4 count_main_clk0_carry__0
       (.CI(count_main_clk0_carry_n_0),
        .CO({NLW_count_main_clk0_carry__0_CO_UNCONNECTED[3:2],count_main_clk0,count_main_clk0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_main_clk0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,count_main_clk0_carry__0_i_1_n_0,count_main_clk0_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    count_main_clk0_carry__0_i_1
       (.I0(FrameSize[15]),
        .O(count_main_clk0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_main_clk0_carry__0_i_2
       (.I0(FrameSize[14]),
        .I1(FrameSize[13]),
        .I2(FrameSize[12]),
        .O(count_main_clk0_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_main_clk0_carry_i_1
       (.I0(FrameSize[11]),
        .I1(FrameSize[10]),
        .I2(FrameSize[9]),
        .O(count_main_clk0_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_main_clk0_carry_i_2
       (.I0(FrameSize[8]),
        .I1(FrameSize[7]),
        .I2(FrameSize[6]),
        .O(count_main_clk0_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_main_clk0_carry_i_3
       (.I0(FrameSize[5]),
        .I1(FrameSize[4]),
        .I2(FrameSize[3]),
        .O(count_main_clk0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h0009)) 
    count_main_clk0_carry_i_4
       (.I0(count_main_clk),
        .I1(FrameSize[0]),
        .I2(FrameSize[2]),
        .I3(FrameSize[1]),
        .O(count_main_clk0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h8804)) 
    count_main_clk_i_1
       (.I0(count_main_clk),
        .I1(reset),
        .I2(count_main_clk0),
        .I3(UNCONN_OUT),
        .O(count_main_clk_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    count_main_clk_reg
       (.C(main_clk),
        .CE(1'b1),
        .D(count_main_clk_i_1_n_0),
        .Q(count_main_clk),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hEF)) 
    state_WR
       (.I0(count_main_clk0),
        .I1(UNCONN_OUT),
        .I2(reset),
        .O(state_WR_n_0));
  LUT3 #(
    .INIT(8'h2F)) 
    \state_WR_inferred__0/i_ 
       (.I0(UNCONN_OUT),
        .I1(count_data_ready_from_ADC0),
        .I2(reset),
        .O(\state_WR_inferred__0/i__n_0 ));
  FDRE state_WR_reg
       (.C(main_clk),
        .CE(1'b1),
        .D(state_WR_n_0),
        .Q(UNCONN_OUT),
        .R(1'b0));
  FDRE state_WR_reg__0
       (.C(data_ready),
        .CE(1'b1),
        .D(\state_WR_inferred__0/i__n_0 ),
        .Q(UNCONN_OUT),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
