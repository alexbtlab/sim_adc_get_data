// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Jun  9 16:09:28 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ram_4096_0_0_sim_netlist.v
// Design      : design_1_ram_4096_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ram_4096_0_0,ram,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ram,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (i_clk,
    i_addr,
    i_write,
    i_data,
    o_data);
  input i_clk;
  input [15:0]i_addr;
  input i_write;
  input [31:0]i_data;
  output [31:0]o_data;

  wire [15:0]i_addr;
  wire i_clk;
  wire [31:0]i_data;
  wire i_write;
  wire [31:0]o_data;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram inst
       (.i_addr(i_addr[12:0]),
        .i_clk(i_clk),
        .i_data(i_data),
        .i_write(i_write),
        .o_data(o_data));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram
   (o_data,
    i_clk,
    i_addr,
    i_data,
    i_write);
  output [31:0]o_data;
  input i_clk;
  input [12:0]i_addr;
  input [31:0]i_data;
  input i_write;

  wire [12:0]i_addr;
  wire i_clk;
  wire [31:0]i_data;
  wire i_write;
  wire [31:0]o_data;
  wire NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_0_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_0_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_0_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_0_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_1_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_1_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_1_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_2_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_2_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_2_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_2_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_2_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_3_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_3_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_3_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_3_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_3_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_4_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_4_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_4_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_4_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_4_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_5_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_5_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_5_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_5_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_5_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_6_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_6_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_6_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_6_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_6_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_7_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_7_SBITERR_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_7_DOADO_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_7_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_7_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "3" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "3" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_0
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[3:0]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_0_DOADO_UNCONNECTED[31:4],o_data[3:0]}),
        .DOBDO(NLW_memory_array_reg_0_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_0_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_0_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "4" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_1
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[7:4]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_1_DOADO_UNCONNECTED[31:4],o_data[7:4]}),
        .DOBDO(NLW_memory_array_reg_1_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_1_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "8" *) 
  (* bram_slice_end = "11" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "11" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_2
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_2_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[11:8]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_2_DOADO_UNCONNECTED[31:4],o_data[11:8]}),
        .DOBDO(NLW_memory_array_reg_2_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_2_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_2_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "12" *) 
  (* bram_slice_end = "15" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_3
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_3_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[15:12]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_3_DOADO_UNCONNECTED[31:4],o_data[15:12]}),
        .DOBDO(NLW_memory_array_reg_3_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_3_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_3_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "16" *) 
  (* bram_slice_end = "19" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "19" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_4
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_4_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[19:16]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_4_DOADO_UNCONNECTED[31:4],o_data[19:16]}),
        .DOBDO(NLW_memory_array_reg_4_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_4_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_4_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "20" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_5
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_5_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[23:20]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_5_DOADO_UNCONNECTED[31:4],o_data[23:20]}),
        .DOBDO(NLW_memory_array_reg_5_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_5_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_5_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "24" *) 
  (* bram_slice_end = "27" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "27" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_6
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_6_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[27:24]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_6_DOADO_UNCONNECTED[31:4],o_data[27:24]}),
        .DOBDO(NLW_memory_array_reg_6_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_6_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_6_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "28" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "28" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("PERFORMANCE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(0),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("NO_CHANGE"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(0)) 
    memory_array_reg_7
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b0),
        .CASCADEOUTA(NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(i_clk),
        .CLKBWRCLK(1'b0),
        .DBITERR(NLW_memory_array_reg_7_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_data[31:28]}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b1,1'b1,1'b1,1'b1}),
        .DOADO({NLW_memory_array_reg_7_DOADO_UNCONNECTED[31:4],o_data[31:28]}),
        .DOBDO(NLW_memory_array_reg_7_DOBDO_UNCONNECTED[31:0]),
        .DOPADOP(NLW_memory_array_reg_7_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b0),
        .INJECTDBITERR(NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_7_SBITERR_UNCONNECTED),
        .WEA({i_write,i_write,i_write,i_write}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
