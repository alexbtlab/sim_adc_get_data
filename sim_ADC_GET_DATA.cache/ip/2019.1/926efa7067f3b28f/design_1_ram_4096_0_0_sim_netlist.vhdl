-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  9 16:09:28 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ram_4096_0_0_sim_netlist.vhdl
-- Design      : design_1_ram_4096_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram is
  port (
    o_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    i_clk : in STD_LOGIC;
    i_addr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    i_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    i_write : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram is
  signal NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_2_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_2_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_3_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_3_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_4_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_4_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_5_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_5_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_6_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_6_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_7_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_7_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_0 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of memory_array_reg_0 : label is 262144;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of memory_array_reg_0 : label is "memory_array";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of memory_array_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of memory_array_reg_0 : label is 8191;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of memory_array_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of memory_array_reg_0 : label is 3;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of memory_array_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of memory_array_reg_0 : label is 8191;
  attribute ram_offset : integer;
  attribute ram_offset of memory_array_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of memory_array_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of memory_array_reg_0 : label is 3;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_1 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_1 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_1 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_1 : label is 0;
  attribute bram_addr_end of memory_array_reg_1 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_1 : label is 4;
  attribute bram_slice_end of memory_array_reg_1 : label is 7;
  attribute ram_addr_begin of memory_array_reg_1 : label is 0;
  attribute ram_addr_end of memory_array_reg_1 : label is 8191;
  attribute ram_offset of memory_array_reg_1 : label is 0;
  attribute ram_slice_begin of memory_array_reg_1 : label is 4;
  attribute ram_slice_end of memory_array_reg_1 : label is 7;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_2 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_2 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_2 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_2 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_2 : label is 0;
  attribute bram_addr_end of memory_array_reg_2 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_2 : label is 8;
  attribute bram_slice_end of memory_array_reg_2 : label is 11;
  attribute ram_addr_begin of memory_array_reg_2 : label is 0;
  attribute ram_addr_end of memory_array_reg_2 : label is 8191;
  attribute ram_offset of memory_array_reg_2 : label is 0;
  attribute ram_slice_begin of memory_array_reg_2 : label is 8;
  attribute ram_slice_end of memory_array_reg_2 : label is 11;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_3 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_3 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_3 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_3 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_3 : label is 0;
  attribute bram_addr_end of memory_array_reg_3 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_3 : label is 12;
  attribute bram_slice_end of memory_array_reg_3 : label is 15;
  attribute ram_addr_begin of memory_array_reg_3 : label is 0;
  attribute ram_addr_end of memory_array_reg_3 : label is 8191;
  attribute ram_offset of memory_array_reg_3 : label is 0;
  attribute ram_slice_begin of memory_array_reg_3 : label is 12;
  attribute ram_slice_end of memory_array_reg_3 : label is 15;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_4 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_4 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_4 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_4 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_4 : label is 0;
  attribute bram_addr_end of memory_array_reg_4 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_4 : label is 16;
  attribute bram_slice_end of memory_array_reg_4 : label is 19;
  attribute ram_addr_begin of memory_array_reg_4 : label is 0;
  attribute ram_addr_end of memory_array_reg_4 : label is 8191;
  attribute ram_offset of memory_array_reg_4 : label is 0;
  attribute ram_slice_begin of memory_array_reg_4 : label is 16;
  attribute ram_slice_end of memory_array_reg_4 : label is 19;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_5 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_5 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_5 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_5 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_5 : label is 0;
  attribute bram_addr_end of memory_array_reg_5 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_5 : label is 20;
  attribute bram_slice_end of memory_array_reg_5 : label is 23;
  attribute ram_addr_begin of memory_array_reg_5 : label is 0;
  attribute ram_addr_end of memory_array_reg_5 : label is 8191;
  attribute ram_offset of memory_array_reg_5 : label is 0;
  attribute ram_slice_begin of memory_array_reg_5 : label is 20;
  attribute ram_slice_end of memory_array_reg_5 : label is 23;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_6 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_6 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_6 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_6 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_6 : label is 0;
  attribute bram_addr_end of memory_array_reg_6 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_6 : label is 24;
  attribute bram_slice_end of memory_array_reg_6 : label is 27;
  attribute ram_addr_begin of memory_array_reg_6 : label is 0;
  attribute ram_addr_end of memory_array_reg_6 : label is 8191;
  attribute ram_offset of memory_array_reg_6 : label is 0;
  attribute ram_slice_begin of memory_array_reg_6 : label is 24;
  attribute ram_slice_end of memory_array_reg_6 : label is 27;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_7 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_7 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_7 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_7 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_7 : label is 0;
  attribute bram_addr_end of memory_array_reg_7 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_7 : label is 28;
  attribute bram_slice_end of memory_array_reg_7 : label is 31;
  attribute ram_addr_begin of memory_array_reg_7 : label is 0;
  attribute ram_addr_end of memory_array_reg_7 : label is 8191;
  attribute ram_offset of memory_array_reg_7 : label is 0;
  attribute ram_slice_begin of memory_array_reg_7 : label is 28;
  attribute ram_slice_end of memory_array_reg_7 : label is 31;
begin
memory_array_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(3 downto 0),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_0_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(3 downto 0),
      DOBDO(31 downto 0) => NLW_memory_array_reg_0_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_0_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(7 downto 4),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_1_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(7 downto 4),
      DOBDO(31 downto 0) => NLW_memory_array_reg_1_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_2: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_2_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(11 downto 8),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_2_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(11 downto 8),
      DOBDO(31 downto 0) => NLW_memory_array_reg_2_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_2_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_2_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_3: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_3_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(15 downto 12),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_3_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(15 downto 12),
      DOBDO(31 downto 0) => NLW_memory_array_reg_3_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_3_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_3_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_4: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_4_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(19 downto 16),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_4_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(19 downto 16),
      DOBDO(31 downto 0) => NLW_memory_array_reg_4_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_4_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_4_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_5: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_5_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(23 downto 20),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_5_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(23 downto 20),
      DOBDO(31 downto 0) => NLW_memory_array_reg_5_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_5_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_5_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_6: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_6_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(27 downto 24),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_6_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(27 downto 24),
      DOBDO(31 downto 0) => NLW_memory_array_reg_6_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_6_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_6_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_7: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 0,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "NO_CHANGE",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 0
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15 downto 0) => B"1111111111111111",
      CASCADEINA => '1',
      CASCADEINB => '0',
      CASCADEOUTA => NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => i_clk,
      CLKBWRCLK => '0',
      DBITERR => NLW_memory_array_reg_7_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => i_data(31 downto 28),
      DIBDI(31 downto 0) => B"11111111111111111111111111111111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"1111",
      DOADO(31 downto 4) => NLW_memory_array_reg_7_DOADO_UNCONNECTED(31 downto 4),
      DOADO(3 downto 0) => o_data(31 downto 28),
      DOBDO(31 downto 0) => NLW_memory_array_reg_7_DOBDO_UNCONNECTED(31 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_7_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_7_SBITERR_UNCONNECTED,
      WEA(3) => i_write,
      WEA(2) => i_write,
      WEA(1) => i_write,
      WEA(0) => i_write,
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    i_clk : in STD_LOGIC;
    i_addr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    i_write : in STD_LOGIC;
    i_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    o_data : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ram_4096_0_0,ram,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ram,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ram
     port map (
      i_addr(12 downto 0) => i_addr(12 downto 0),
      i_clk => i_clk,
      i_data(31 downto 0) => i_data(31 downto 0),
      i_write => i_write,
      o_data(31 downto 0) => o_data(31 downto 0)
    );
end STRUCTURE;
