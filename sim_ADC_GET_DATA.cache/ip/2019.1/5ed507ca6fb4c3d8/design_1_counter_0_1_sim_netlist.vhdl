-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jun  9 16:08:17 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_counter_0_1_sim_netlist.vhdl
-- Design      : design_1_counter_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter is
  port (
    count : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter is
  signal clear : STD_LOGIC;
  signal \cnt0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt0_carry_i_1__0_n_0\ : STD_LOGIC;
  signal \cnt0_carry_i_1__1_n_0\ : STD_LOGIC;
  signal cnt0_carry_i_1_n_0 : STD_LOGIC;
  signal \cnt0_carry_i_2__0_n_0\ : STD_LOGIC;
  signal \cnt0_carry_i_2__1_n_0\ : STD_LOGIC;
  signal cnt0_carry_i_2_n_0 : STD_LOGIC;
  signal \cnt0_carry_i_3__0_n_0\ : STD_LOGIC;
  signal \cnt0_carry_i_3__1_n_0\ : STD_LOGIC;
  signal cnt0_carry_i_3_n_0 : STD_LOGIC;
  signal \cnt0_carry_i_4__0_n_0\ : STD_LOGIC;
  signal cnt0_carry_i_4_n_0 : STD_LOGIC;
  signal cnt0_carry_n_0 : STD_LOGIC;
  signal cnt0_carry_n_1 : STD_LOGIC;
  signal cnt0_carry_n_2 : STD_LOGIC;
  signal cnt0_carry_n_3 : STD_LOGIC;
  signal \cnt[3]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \^count\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_cnt0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  count(31 downto 0) <= \^count\(31 downto 0);
cnt0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt0_carry_n_0,
      CO(2) => cnt0_carry_n_1,
      CO(1) => cnt0_carry_n_2,
      CO(0) => cnt0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_cnt0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => cnt0_carry_i_1_n_0,
      S(2) => cnt0_carry_i_2_n_0,
      S(1) => cnt0_carry_i_3_n_0,
      S(0) => cnt0_carry_i_4_n_0
    );
\cnt0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt0_carry_n_0,
      CO(3) => \cnt0_carry__0_n_0\,
      CO(2) => \cnt0_carry__0_n_1\,
      CO(1) => \cnt0_carry__0_n_2\,
      CO(0) => \cnt0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_cnt0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \cnt0_carry_i_1__1_n_0\,
      S(2) => \cnt0_carry_i_2__1_n_0\,
      S(1) => \cnt0_carry_i_3__0_n_0\,
      S(0) => \cnt0_carry_i_4__0_n_0\
    );
\cnt0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__0_n_0\,
      CO(3) => \NLW_cnt0_carry__1_CO_UNCONNECTED\(3),
      CO(2) => clear,
      CO(1) => \cnt0_carry__1_n_2\,
      CO(0) => \cnt0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_cnt0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \cnt0_carry_i_1__0_n_0\,
      S(1) => \cnt0_carry_i_2__0_n_0\,
      S(0) => \cnt0_carry_i_3__1_n_0\
    );
cnt0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^count\(9),
      I1 => FrameSize(9),
      I2 => FrameSize(11),
      I3 => \^count\(11),
      I4 => FrameSize(10),
      I5 => \^count\(10),
      O => cnt0_carry_i_1_n_0
    );
\cnt0_carry_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^count\(30),
      I1 => \^count\(31),
      O => \cnt0_carry_i_1__0_n_0\
    );
\cnt0_carry_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^count\(23),
      I1 => \^count\(22),
      I2 => \^count\(21),
      O => \cnt0_carry_i_1__1_n_0\
    );
cnt0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^count\(6),
      I1 => FrameSize(6),
      I2 => FrameSize(8),
      I3 => \^count\(8),
      I4 => FrameSize(7),
      I5 => \^count\(7),
      O => cnt0_carry_i_2_n_0
    );
\cnt0_carry_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^count\(29),
      I1 => \^count\(28),
      I2 => \^count\(27),
      O => \cnt0_carry_i_2__0_n_0\
    );
\cnt0_carry_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^count\(20),
      I1 => \^count\(19),
      I2 => \^count\(18),
      O => \cnt0_carry_i_2__1_n_0\
    );
cnt0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^count\(3),
      I1 => FrameSize(3),
      I2 => FrameSize(5),
      I3 => \^count\(5),
      I4 => FrameSize(4),
      I5 => \^count\(4),
      O => cnt0_carry_i_3_n_0
    );
\cnt0_carry_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0009"
    )
        port map (
      I0 => \^count\(15),
      I1 => FrameSize(15),
      I2 => \^count\(17),
      I3 => \^count\(16),
      O => \cnt0_carry_i_3__0_n_0\
    );
\cnt0_carry_i_3__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^count\(26),
      I1 => \^count\(25),
      I2 => \^count\(24),
      O => \cnt0_carry_i_3__1_n_0\
    );
cnt0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^count\(0),
      I1 => FrameSize(0),
      I2 => FrameSize(2),
      I3 => \^count\(2),
      I4 => FrameSize(1),
      I5 => \^count\(1),
      O => cnt0_carry_i_4_n_0
    );
\cnt0_carry_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^count\(12),
      I1 => FrameSize(12),
      I2 => FrameSize(14),
      I3 => \^count\(14),
      I4 => FrameSize(13),
      I5 => \^count\(13),
      O => \cnt0_carry_i_4__0_n_0\
    );
\cnt[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^count\(0),
      O => \cnt[3]_i_2_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[3]_i_1_n_7\,
      Q => \^count\(0),
      R => clear
    );
\cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[11]_i_1_n_5\,
      Q => \^count\(10),
      R => clear
    );
\cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[11]_i_1_n_4\,
      Q => \^count\(11),
      R => clear
    );
\cnt_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[7]_i_1_n_0\,
      CO(3) => \cnt_reg[11]_i_1_n_0\,
      CO(2) => \cnt_reg[11]_i_1_n_1\,
      CO(1) => \cnt_reg[11]_i_1_n_2\,
      CO(0) => \cnt_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[11]_i_1_n_4\,
      O(2) => \cnt_reg[11]_i_1_n_5\,
      O(1) => \cnt_reg[11]_i_1_n_6\,
      O(0) => \cnt_reg[11]_i_1_n_7\,
      S(3 downto 0) => \^count\(11 downto 8)
    );
\cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[15]_i_1_n_7\,
      Q => \^count\(12),
      R => clear
    );
\cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[15]_i_1_n_6\,
      Q => \^count\(13),
      R => clear
    );
\cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[15]_i_1_n_5\,
      Q => \^count\(14),
      R => clear
    );
\cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[15]_i_1_n_4\,
      Q => \^count\(15),
      R => clear
    );
\cnt_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[11]_i_1_n_0\,
      CO(3) => \cnt_reg[15]_i_1_n_0\,
      CO(2) => \cnt_reg[15]_i_1_n_1\,
      CO(1) => \cnt_reg[15]_i_1_n_2\,
      CO(0) => \cnt_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[15]_i_1_n_4\,
      O(2) => \cnt_reg[15]_i_1_n_5\,
      O(1) => \cnt_reg[15]_i_1_n_6\,
      O(0) => \cnt_reg[15]_i_1_n_7\,
      S(3 downto 0) => \^count\(15 downto 12)
    );
\cnt_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[19]_i_1_n_7\,
      Q => \^count\(16),
      R => clear
    );
\cnt_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[19]_i_1_n_6\,
      Q => \^count\(17),
      R => clear
    );
\cnt_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[19]_i_1_n_5\,
      Q => \^count\(18),
      R => clear
    );
\cnt_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[19]_i_1_n_4\,
      Q => \^count\(19),
      R => clear
    );
\cnt_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[15]_i_1_n_0\,
      CO(3) => \cnt_reg[19]_i_1_n_0\,
      CO(2) => \cnt_reg[19]_i_1_n_1\,
      CO(1) => \cnt_reg[19]_i_1_n_2\,
      CO(0) => \cnt_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[19]_i_1_n_4\,
      O(2) => \cnt_reg[19]_i_1_n_5\,
      O(1) => \cnt_reg[19]_i_1_n_6\,
      O(0) => \cnt_reg[19]_i_1_n_7\,
      S(3 downto 0) => \^count\(19 downto 16)
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[3]_i_1_n_6\,
      Q => \^count\(1),
      R => clear
    );
\cnt_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[23]_i_1_n_7\,
      Q => \^count\(20),
      R => clear
    );
\cnt_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[23]_i_1_n_6\,
      Q => \^count\(21),
      R => clear
    );
\cnt_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[23]_i_1_n_5\,
      Q => \^count\(22),
      R => clear
    );
\cnt_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[23]_i_1_n_4\,
      Q => \^count\(23),
      R => clear
    );
\cnt_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[19]_i_1_n_0\,
      CO(3) => \cnt_reg[23]_i_1_n_0\,
      CO(2) => \cnt_reg[23]_i_1_n_1\,
      CO(1) => \cnt_reg[23]_i_1_n_2\,
      CO(0) => \cnt_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[23]_i_1_n_4\,
      O(2) => \cnt_reg[23]_i_1_n_5\,
      O(1) => \cnt_reg[23]_i_1_n_6\,
      O(0) => \cnt_reg[23]_i_1_n_7\,
      S(3 downto 0) => \^count\(23 downto 20)
    );
\cnt_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[27]_i_1_n_7\,
      Q => \^count\(24),
      R => clear
    );
\cnt_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[27]_i_1_n_6\,
      Q => \^count\(25),
      R => clear
    );
\cnt_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[27]_i_1_n_5\,
      Q => \^count\(26),
      R => clear
    );
\cnt_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[27]_i_1_n_4\,
      Q => \^count\(27),
      R => clear
    );
\cnt_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[23]_i_1_n_0\,
      CO(3) => \cnt_reg[27]_i_1_n_0\,
      CO(2) => \cnt_reg[27]_i_1_n_1\,
      CO(1) => \cnt_reg[27]_i_1_n_2\,
      CO(0) => \cnt_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[27]_i_1_n_4\,
      O(2) => \cnt_reg[27]_i_1_n_5\,
      O(1) => \cnt_reg[27]_i_1_n_6\,
      O(0) => \cnt_reg[27]_i_1_n_7\,
      S(3 downto 0) => \^count\(27 downto 24)
    );
\cnt_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[31]_i_1_n_7\,
      Q => \^count\(28),
      R => clear
    );
\cnt_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[31]_i_1_n_6\,
      Q => \^count\(29),
      R => clear
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[3]_i_1_n_5\,
      Q => \^count\(2),
      R => clear
    );
\cnt_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[31]_i_1_n_5\,
      Q => \^count\(30),
      R => clear
    );
\cnt_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[31]_i_1_n_4\,
      Q => \^count\(31),
      R => clear
    );
\cnt_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[27]_i_1_n_0\,
      CO(3) => \NLW_cnt_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_reg[31]_i_1_n_1\,
      CO(1) => \cnt_reg[31]_i_1_n_2\,
      CO(0) => \cnt_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[31]_i_1_n_4\,
      O(2) => \cnt_reg[31]_i_1_n_5\,
      O(1) => \cnt_reg[31]_i_1_n_6\,
      O(0) => \cnt_reg[31]_i_1_n_7\,
      S(3 downto 0) => \^count\(31 downto 28)
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[3]_i_1_n_4\,
      Q => \^count\(3),
      R => clear
    );
\cnt_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_reg[3]_i_1_n_0\,
      CO(2) => \cnt_reg[3]_i_1_n_1\,
      CO(1) => \cnt_reg[3]_i_1_n_2\,
      CO(0) => \cnt_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_reg[3]_i_1_n_4\,
      O(2) => \cnt_reg[3]_i_1_n_5\,
      O(1) => \cnt_reg[3]_i_1_n_6\,
      O(0) => \cnt_reg[3]_i_1_n_7\,
      S(3 downto 1) => \^count\(3 downto 1),
      S(0) => \cnt[3]_i_2_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[7]_i_1_n_7\,
      Q => \^count\(4),
      R => clear
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[7]_i_1_n_6\,
      Q => \^count\(5),
      R => clear
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[7]_i_1_n_5\,
      Q => \^count\(6),
      R => clear
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[7]_i_1_n_4\,
      Q => \^count\(7),
      R => clear
    );
\cnt_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[3]_i_1_n_0\,
      CO(3) => \cnt_reg[7]_i_1_n_0\,
      CO(2) => \cnt_reg[7]_i_1_n_1\,
      CO(1) => \cnt_reg[7]_i_1_n_2\,
      CO(0) => \cnt_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[7]_i_1_n_4\,
      O(2) => \cnt_reg[7]_i_1_n_5\,
      O(1) => \cnt_reg[7]_i_1_n_6\,
      O(0) => \cnt_reg[7]_i_1_n_7\,
      S(3 downto 0) => \^count\(7 downto 4)
    );
\cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[11]_i_1_n_7\,
      Q => \^count\(8),
      R => clear
    );
\cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \cnt_reg[11]_i_1_n_6\,
      Q => \^count\(9),
      R => clear
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ready : out STD_LOGIC;
    count : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_counter_0_1,counter,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "counter,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^clk\ : STD_LOGIC;
begin
  \^clk\ <= clk;
  ready <= \^clk\;
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      clk => \^clk\,
      count(31 downto 0) => count(31 downto 0)
    );
end STRUCTURE;
