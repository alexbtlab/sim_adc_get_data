vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm
vlib riviera/xlconstant_v1_1_6

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm
vmap xlconstant_v1_1_6 riviera/xlconstant_v1_1_6

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../sim_ADC_GET_DATA.srcs/sources_1/bd/design_1/ipshared/c923" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../sim_ADC_GET_DATA.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../bd/design_1/ipshared/de74/hdl/counter_v1_0.v" \
"../../../bd/design_1/ip/design_1_counter_0_1/sim/design_1_counter_0_1.v" \
"../../../bd/design_1/ipshared/0d68/hdl/ram_control_v1_0.v" \
"../../../bd/design_1/ip/design_1_ram_control_0_0/sim/design_1_ram_control_0_0.v" \
"../../../bd/design_1/ipshared/8ca9/hdl/ram_4096_v1_0.v" \
"../../../bd/design_1/ip/design_1_ram_4096_0_0/sim/design_1_ram_4096_0_0.v" \

vlog -work xlconstant_v1_1_6  -v2k5 "+incdir+../../../../sim_ADC_GET_DATA.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../../sim_ADC_GET_DATA.srcs/sources_1/bd/design_1/ipshared/66e7/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../sim_ADC_GET_DATA.srcs/sources_1/bd/design_1/ipshared/c923" \
"../../../bd/design_1/ip/design_1_xlconstant_0_0/sim/design_1_xlconstant_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0_clk_wiz.v" \
"../../../bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0.v" \
"../../../bd/design_1/ipshared/418b/hdl/sampleGenerator_v1_0_M00_AXIS.v" \
"../../../bd/design_1/ipshared/418b/hdl/sampleGenerator_v1_0.v" \
"../../../bd/design_1/ip/design_1_sampleGenerator_0_1/sim/design_1_sampleGenerator_0_1.v" \
"../../../bd/design_1/sim/design_1.v" \

vlog -work xil_defaultlib \
"glbl.v"

