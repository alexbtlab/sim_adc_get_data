
`timescale 1 ns / 1 ps

	module ram_control(
		input wire   main_clk,
		input wire   reset,
		input wire   data_ready,
		input wire  [15:0] FrameSize,
		output wire  clk_write_read,
		output reg  [15:0]  adr_ram,
		output wire  WR,
		output wire  clk_100MHz_AXI
	);
	
	reg  [15:0] count_clk_write_read = 0;
	reg  state_WR;
/*-----------------------------------------------------------------------------------------------------*/
always @ (negedge clk_write_read) begin
    if(!reset) begin
        adr_ram  =  0;
    end
    else begin
        adr_ram <= count_clk_write_read; 
    end             
end
/*-----------------------------------------------------------------------------------------------------*/
always @ (posedge clk_write_read) begin
    if(!reset) begin
        count_clk_write_read <= 0;
        state_WR = 1;
    end
    else begin
        if(state_WR) begin 
            if(count_clk_write_read == (FrameSize + 1)) begin
                count_clk_write_read <= 0;
                state_WR = 0;  
            end
            else begin
                count_clk_write_read <= count_clk_write_read + 1;
            end 
        end
        else if(!state_WR) begin 
            if(count_clk_write_read == (FrameSize + 1)) begin
                count_clk_write_read <= 0;
                state_WR = 1;  
            end
            else begin
                count_clk_write_read <= count_clk_write_read + 1;
            end 
        end                      
    end    
end
/*-----------------------------------------------------------------------------------------------------*/
assign clk_write_read = (state_WR) ? ~data_ready : ~main_clk;
assign WR             = (state_WR) ? 1 : 0;
//assign adr_ram        = (state_WR) ? count_data_ready_from_ADC : count_main_clk;
//assign clk_100MHz_AXI = (state_WR | (count_main_clk == 0)) ? 0 : main_clk;
assign clk_100MHz_AXI = (state_WR |  (count_clk_write_read == 0) | (count_clk_write_read ==  1)) ? 0 : main_clk;
//assign clk_write_read


endmodule
